# COMP2300 lab 7 template

<https://cs.anu.edu.au/courses/comp2300/labs/07-functions/>

If you have any questions, ask them on
[Piazza](https://piazza.com/anu.edu.au/spring2018/comp23006300/home).
